package com.example.restapi2.repository;

import com.example.restapi2.entity.Product;
import com.example.restapi2.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Integer> {
    Product findByName(String name);
}
